FROM ubuntu
RUN apt-get update -qy && apt-get install -y python3-dev python3-pip
COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip3 install -r requirements.txt
COPY . /app 
EXPOSE 5000
ENTRYPOINT ["python3"]
CMD ["text_processing.py"]
