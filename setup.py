import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="edataprocessing",
    version="0.0.1",
    author="priya",
    author_email="priravindran@deloitte.com",
    description="NLP Text processing",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/dataprocessing",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)